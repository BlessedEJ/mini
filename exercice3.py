__author__ = 'Jean'

import string

punctuations = [mark for mark in string.punctuation]
whitespaces = [space for space in string.whitespace]

origin = 'origin.txt'


books = [origin]

def words(book):
    list_ = []
    flag = False
    signal = "*** START OF"
    op = open(book, 'r')
    for line in op:
        if flag == True:
            for word in line.split():
                list_.append(word)
        elif (signal in line) and (flag == False):
            flag = True
        else:
            pass
    op.close()
    return list_


def clean(word):
     result = ''
     for char in word:
         if (char in whitespaces) or (char in punctuations):
             pass
         else:
             result += char.lower()
     return result

def histogram(data):
     hist = {}
     for word in data:
         if word == '':
             pass
         else:
             hist[word] = hist.get(word, 0) + 1
     return hist

def main():
    for book in books:
        data = [clean(word) for word in words(book)]
        print "Les 20 mots du livre %s sont:" % book
        hist = histogram(data)
        top20 = []
        for key in hist:
            top20.append([hist[key], key])
        top20.sort(reverse=True)
        for i in range(0, 20):
            print "  %s) %s %s" % (i + 1, top20[i][1], top20[i][0])

        print "\n"

main()