__author__ = 'Jean'
import string
from random import *
from bisect import bisect_left


def process_file(filename):
    h = dict()
    fp = open(filename)
    for line in fp:
        process_line(line, h)
    return h


def process_line(line, h):
    line = line.replace('-', ' ')
    words = line.split()
    for i in range(len(words)):
        if i < len(words) - 1:
            prefix = stripping(words[i])
            next = h.get(prefix, dict())
            word2 = stripping(words[i + 1])
            next[word2] = next.get(word2, 0) + 1
            h[prefix] = next


def stripping(word):
    new = word.strip(string.punctuation + string.whitespace)
    new = new.lower()
    return new


def cumsum(di):
    cumlist = []
    numlist = []
    for suffix, freq in di:
        numlist.append(freq)
    for i in range(len(numlist)):
        if i == 0:
            cumlist.append(numlist[i])
        else:
            cumlist.append(cumlist[i - 1] + numlist[i])
    return cumlist


def getRandom(filename):
    hist = process_file(filename)
    wordList = hist.keys()
    word = choice(wordList)
    words = word
    for i in range(11):
        next = nextRandom(word, hist)
        words = words + ' ' + next
        word = next
    return words


def nextRandom(word, di):
    wordList = di[word].keys()
    cumList = cumsum(di[word].items())
    r = randint(1, cumList[-1])
    pos = bisect_left(cumList, r)
    return wordList[pos]


if __name__ == '__main__':
    print getRandom('origin.txt')