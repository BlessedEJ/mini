__author__ = 'Jean'
import string


def lecture(filename):

    fichier = open(filename)

    for ligne in fichier:
        for mots in ligne.split():
            mts = mots.strip(string.punctuation + string.whitespace)
            mts = mts.lower()

            print(mts)


if __name__ == '__main__':
    filename = 'glo.txt'

    lecture(filename)
