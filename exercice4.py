__author__ = 'Jean'
import string
import random

def process_file(filename, skip_header=False):

    hist = {}
    fp = open(filename)

    if skip_header:
        skip_gutenberg_header(fp)

    for line in fp:
        process_line(line, hist)
    return hist

def most_used(hist):
    #retourne les mots les plus utilises dans l'histogram
    t = []
    for key, value in hist.items():
        t.append((value, key))

    t.sort(reverse=True)

    return t

def total_words(hist):
    #Retourne la valeur des mots
    return sum(hist.values())

def different_words(hist):
    #retourne la difference des mots
    return len(hist)

def print_most_used(hist, num=10):
    #affiche les mots les plus utilises et leur valeurs

    t = most_used(hist)
    print("Les mots les plus communs sont:")
    for freq, word in t[:num]:
        print(word, ' ', freq)

def process_line(line, hist):
   #ajout des mots dans l'histogram
    line = line.replace('-', ' ')

    for word in line.split():
        word = word.strip(string.punctuation + string.whitespace)
        word = word.lower()

        hist[word] = hist.get(word, 0) + 1

def skip_gutenberg_header(fp):
    
    for line in fp:
        if line.startswith('*END*THE SMALL PRINT!'):
            break

def subtract(d1,d2):
    res = dict()
    for key in d1:
        if key not in d2:
            res[key] = None
    return res


def choose_from_random(hist):
    t = []
    for word, freq in hist.items():
        t.extend([word] * freq)
    return random.choice(t)



if __name__ == '__main__':
    filename = 'emma.txt'

    hist = process_file(filename, True)

    print_most_used(hist, 12)

    words = process_file('words.txt')
    diff = subtract(hist, words)
    print("Les mots qui sont dans le livre et qui ne sont pas dans la liste:")
    for word in diff.keys():
        print('word, end = ''')

    print(choose_from_random(hist))
    for key, val in words.items():
        print(key, val)
