__author__ = 'Jean'
import string


def lecture_fichier(filename,skip_header=False):

    hist = {}
    fp = open(filename)

    for ligne in fp:
        lecture_ligne(ligne, hist)

    return hist

def lecture_ligne(ligne, hist):
    """ajout des mots dans l'histogram"""
    ligne = ligne.replace('-', ' ')

    for mots in ligne.split():
        mts = mots.strip(string.punctuation + string.whitespace)
        mts = mts.lower()

        hist[mots] = hist.get(mts, 0) + 1

if __name__ == '__main__':
    filename = 'glo.txt'

    mts = lecture_fichier(filename)

    for key, val in mts.items():
        print(key, val)
